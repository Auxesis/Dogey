﻿namespace Dogey
{
    public class MessagePoint : Point
    {
        public ulong MessageId { get; set; }
    }
}
