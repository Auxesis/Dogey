﻿namespace Dogey
{
    public class Cost
    {
        public string Id { get; set; }
        public ulong Amount { get; set; } = 0;
    }
}
