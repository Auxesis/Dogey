﻿namespace Dogey
{
    public class UserPoint : Point
    {
        public ulong PayerId { get; set; }
        public string Reason { get; set; }
    }
}
