﻿namespace Dogey
{
    public class DogImage
    {
        public ulong Id { get; set; }
        public ulong ChannelId { get; set; }
        public ulong MessageId { get; set; }
        public string Url { get; set; }
    }
}
