﻿namespace Dogey
{
    public class TagDefaults
    {
        public ulong UserId { get; set; }
        public bool IsCommand { get; set; }
        public bool IsCurrentChannel { get; set; }
    }
}
