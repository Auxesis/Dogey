﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System.Text;
using System.Threading.Tasks;

namespace Dogey.Modules.Points
{
    [Name("Points"), Group("point"), Alias("points", "pt", "pts")]
    public class PointsModule : DogeyModuleBase
    {
        private readonly PointsManager _manager;

        public PointsModule(PointsManager manager)
        {
            _manager = manager;
        }

        [Command]
        [Summary("View your current points profile")]
        public Task PointsAsync()
            => PointsAsync(Context.User);

        [Command]
        [Summary("View the specified user's current points profile")]
        public async Task PointsAsync(SocketUser user)
        {
            var profile = await _manager.GetOrCreateProfileAsync(user.Id);

            var embed = new EmbedBuilder()
                .WithDescription($"{user.Mention} currently has {profile.TotalPoints}/{profile.WalletSize} points.");

            await ReplyAsync(embed);
        }

        //[Command("recent")]
        //[Summary("View your recent points balance changes")]
        //public Task RecentAsync()
        //    => RecentAsync(Context.User);

        //[Command("recent")]
        //[Summary("View the specified user's recent points balance changes")]
        //public async Task RecentAsync(SocketUser user)
        //{
        //    var points = await _manager.GetRecentPointsAsync(user.Id);

        //    var builder = new StringBuilder();
        //    foreach (var point in points)
        //        builder.AppendLine($"({point.MessageId}) **x{point.Amount}**");

        //    var embed = new EmbedBuilder()
        //        .WithTitle($"Recent point changes for {user}")
        //        .WithDescription(builder.ToString());

        //    await ReplyAsync(embed);
        //}
        
        [Command("upgrade")]
        [Summary("Double your wallet size")]
        [Remarks("Upgrading your wallet will double your potential max points (250 to 500, 500 to 1000, etc...), at the cost of a maxed-out wallet.")]
        public async Task UpgradeAsync()
        {
            var builder = new EmbedBuilder();
            var profile = await _manager.GetProfileAsync(Context.User.Id);

            if (profile.IsMaxPoints())
            {
                profile = await _manager.UpgradeWalletAsync(profile);
                
                builder.WithDescription($"{Context.User.Mention}'s wallet size is now {profile.WalletSize}");

                await ReplyAsync(builder);
                return;
            }

            builder.WithDescription($"{Context.User.Mention} needs {profile.WalletSize - profile.TotalPoints} more points to upgrade their wallet.");
            await ReplyAsync(builder);
        }
    }
}
